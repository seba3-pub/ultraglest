#  Reglas del ultraglest

## Características

Es un juego de mesa de estrategia por turnos. Se puede jugar de a 2 o más jugadores (quizás más de 6 no sería práctico). Se trata de estrategia de guerra, administración de recursos y diplomacia.

Los materiales se pueden construir de forma casera.

## Materiales

Si bien se puede construir un tablero, también es posible jugar sobre baldosas, preferentemente exagonales, o probar con algún otro entramado. (El entarimado de tipo "bastones rotos", también llamado "cola de pescado" tiene una topología equivalente a la de baldosas exagonales).

Además se necesita:

* 5 dados.
* Fichas de unidades de cada jugador: fichas con un color o dibujo que identifiquen a cada jugador. Con una docena por jugador debería ser suficiente.
* Fichas de castillo, preferentemente personalizadas: fichas que representan un castillo. Si es posible, que tengan alguna decoración para identificar a cada jugador. Con 8 por jugador debería ser suficiente. También se pueden reservar algunas sin personalización por si alguna vez no son suficientes las personalizadas.
* Fichas de bosque: fichas que representan bosques. Con 8 debería ser suficiente.
* Fichas de recursos: pueden ser más pequeñas que las anteriores. Representan los 2 tipos de recurso: comida y madera. Se recomienda contar con 10 o 12 fichas de madera por jugador, y la mitad de esa cantidad para las fichas de comida.

## Preparación de la partida

Se comienza por ubicar los bosques. La cantidad de bosque se mantiene constante a lo largo de la partida. La cantidad a utilizar deberá ser acorde a la cantidad de jugadores.

La cantidad de bosque no debe ser menor a la cantidad de jugadores y no debe ser mucho mayor. A mayor cantidad de bosques, más larga se hará la partida. Más del doble haría las partidas muy largas.

Luego se procede a ubicar un castillo para cada jugador y una unidad en la misma celda, con el siguiente criterio: todos los jugadores deberán estar a la misma distancia de la misma cantidad de bosques. Por ejemplo: si hay 4 bosques, cada jugador debería estar a 2 casilleros de un bosque y a 2 casilleros de otro bosque.

Se muestran algunos ejemplos:

<div width="100%">
<img alt="exágonos 2 bosques" src="exagonos-2b.png" width="25%" style="padding:20px">
<img alt="exágonos 4 bosques" src="exagonos-4b.png" width="25%" style="padding:20px">
<img alt="exágonos 5 bosques" src="exagonos-5b.png" width="25%" style="padding:20px">
</div>

El orden de los turnos se sortea mediante dados o se decide por común acuerdo.

Finalmente se debe decidir si inicialmente habrán alianzas entre jugadores. Los jugadores aliados comparten la victoria y la derrota, y pueden trabajar armoniosamente contra enemigos en común. Las alianzas también se pueden hacer y deshacer durante el juego.

## Objetivo

El objetivo es destruir los castillos enemigos. Cuando no queden castillos enemigos, el jugador o alianza sobreviviente será el ganador.

## Castillos y territorio

Cada jugador comienza con un castillo, y podrá ir creando castillos nuevos. Los castillos sirven de protección ante ataques enemigos (ver detalles en la sección de combate) y para almacenar recursos. Cuando un jugador se queda sin castillos, no continúa jugando la partida.

Se denomina territorio de un jugador a las celdas ocupadas por sus castillos y las celdas contíguas a estas. Podrían haber territorios superpuestos, es decir, celdas compartidas por el territorio de más de un jugador.

Cada castillo puede contener un máximo de 5 fichas de comida y 5 fichas de madera. Las celdas sin castillo no tienen límite de cantidad de recursos. Cuando se relevan unidades nuevas, estas aparecerán en cualquiera de los castillos propios a elección.

Construir castillos nuevos tiene algunos requisitos:

* El jugador debe entregar 12 fichas de madera. Luego se especifica dónde deben estar esos recursos.
* No se puede construir sobre un bosque.
* No se puede construir sobre celdas ocupadas por enemigos.
* No se puede construir en territorio de un enemigo. Sin embargo sí es posible construir al lado de un territorio enemigo, con lo cual una parte del territorio pasa a ser compartido.
* Si se cumplen las condiciones anteriores el jugador puede construir el castillo en una celda de su territorio no ocupada por castillos o:
* En una celda fuera de su territorio, pero que tenga todas las fichas de madera requeridas y ocupada por al menos una unidad propia o aliada.
* En el primer caso, los recursos deberán encontrarse en cualquier celda de su territorio.

## Acciones en cada turno

Cada turno un jugador puede realizar cualquiera de las siguientes acciones:

* Mover una unidad a una celda contigua (que no contenga enemigos ni castillos enemigos). Al hacerlo, opcionalmente puede transportar hasta 3 fichas de recurso desde la celda en que estaba a la celda de destino.
* Reclutar nueva unidad. La nueva unidad aparecerá un castillo propio a elección.
* Atacar a un enemigo (ver más adelante).
* Reubicar recursos: mover una o más fichas de recursos entre celdas de su territorio, siempre que se respeten los límites de almacenamiento.
* Construir nuevo castillo.
* Diplomacia: deshacer, modificar y/o hacer nuevas alianzas.
* Hacer donaciones o intercambio de recursos con otro jugador de común acuerdo.
* Pasar (no tomar ninguna acción).

## Recursos

Al final de cada turno se revisa la producción y consumo de recursos del turno.

El jugador produce una ficha de recurso (a elección comida o madera) por cada unidad que haya permanecido en los bosques sin moverse ni atacar, con un máximo de 3 unidades explotando una misma celda de bosque. En caso de que haya aliados compartiendo la celda de bosque, estos deberán acordar qué unidades explotarán el bosque, a fin de no exceder el límite. A elección del jugador, cada recurso producido se alojará en cualquier celda de su territorio, o en el bosque donde se produjo.

Además, el jugador consume una ficha de comida por cada dos unidades existentes a partir de dos unidades. (1 unidad no consume, 2 consumen 1, 3 consumen 1, 4 consumen 2, y así). Las fichas requeridas se tomarán de cualquier celda de su territorio (a elección) o de la celda en que se encuentran ambas unidades que lo consumen. Si el jugador no pudiera alimentar a las unidades con estos requisitos, luego de entregar todas las fichas de comida disponibles en su territorio, el jugador debe eliminar una unidad a elección.

En el turno en que se recluta una unidad nueva, esta consume recursos y no produce. (De todos modos no podría producir porque el castillo donde aparece nunca estará en un bosque).

Los recursos que se encuentren fuera de cualquier territorio podrán ser apropiados por cualquier jugador, es decir, ya no pertenecen a su dueño anterior.

## Combate

Cuando un jugador decide usar su turno para atacar, elige a cuál celda ocupada por enemigos o castillo enemigo atacar. Puede elegir con cuáles unidades propias atacar, de entre las que se encuentran en celdas contíguas a la celda a atacar. No es posible atacar con las unidades de un aliado. El aliado deberá esperar su turno para acompañar con un nuevo ataque si lo desea.

El atacante tira tantos dados como unidades participen en el ataque, con un máximo de 5 dados. También se tiran tantos dados como unidades haya bajo ataque, con un máximo de 5 dados. Si la celda bajo ataque tiene un castillo, al puntaje de los atacados se les suma 12.

Si el atacante obtiene menos puntos, elimina una de sus unidades. Si el atacado obtiene menos puntos se procede de la siguiente manera: si hay unidades bajo ataque, se elimina una unidad a elección (de común acuerdo si hubieran alianzas). En caso de que solo hubiera un castillo, el atacante podrá optar por destruirlo o capturarlo. En el primer caso se retira la ficha de castillo y en el segundo se reemplaza por una ficha de castillo del atacante.

En caso de empate, no se elimina ninguna unidad ni castillo.

En caso de que luego del ataque no queden unidades atacadas ni castillos atacados (casos de destrucción de castillo o conquista), los recursos existentes quedarán en el lugar, es decir, no se transportarán a ningún otro castillo.

## Unidades especializadas

Hasta acá se tiene un buen conjunto de reglas. Se recomienda que en sus primeras partidas, los jugadores aplican las reglas explicadas hasta antes de esta sección, para familiarizarse con el juego, dominar la gestión de los recursos, y formarse algunas estrategias. Una vez logrado esto, el juego puede hacerse más interesante incorporando las reglas de las unidades especializadas, que se explican a continuación.

Las unidades no especializadas siguen siendo válidas, pero se agregan las especializadas. Para distinguirlas se pueden utilizar fichas de colores que se colocan sobre la ficha de unidad, (aunque también se pueden diseñar e imprimir nuevos diseños, pero no es realmente necesario). Estas fichas señaladoras pueden ser de color rojo, verde y amarillo, significando respectivamente unidad especializada en ataque, unidad especializada en recolección y unidad especializada en defensa, y si lo prefiere se podría concebir como arqueros, recolectores y escuderos, respectivamente.

Al producir una unidad se puede optar por que esta sea especializada, entregando a cambio una ficha de madera. La unidad con la que se comienza el juego no es especializada.

De las unidades que se encuentran explotando un bosque, las que sean especializadas en recolección duplican su capacidad de recolección. De manera que cada unidad especializada en recolección, en las condiciones explicadas antes recolectan dos unidades de recurso por turno.  Las unidades especializadas en recolección, además tienen mayor capacidad que el resto para transportar recursos. Pueden transportar hasta 5 fichas de recurso al desplazarse.

Para el combate las unidades especializadas en recolección no son más hábiles que las unidades no especializadas. Por el contrario las especializadas en ataque o defensa tienen habilidades especiales que dependerán del rol que tomen en el combate (ataque o defensa).

Al lanzar los dados, ya sea para atacar o defender, el jugador deberá ser claro con qué dado corresponde a qué unidad. Para esto se puede proceder de la siguiente manera:

* El atacante primero lanza los dados correspondientes a las unidades especializadas en ataque participantes del ataque, coloca los dados al lado de estas, y luego lanza los dados correspondientes al resto de las unidades y los coloca al lado de cada una. 
* El defensor primero lanza los dados correspondientes a las unidades especializadas en defensa participantes de la defensa, coloca los dados al lado de estas, y luego lanza los dados correspondientes al resto de las unidades y los coloca al lado de cada una. 

Cuando un jugador pierde una unidad en combate, él puede elegir cuál de las unidades participantes del combate morirá.

Para el cálculo del puntaje en combate:

* Para el jugador que está atacando, el valor de cada dado de las unidades especializadas en ataque, vale doble. El valor del resto de los dados (de unidades no especializadas en ataque) simplemente se suma como antes.
* Para el jugador que está defendiendo, el valor de cada dado de las unidades especializadas en defensa, vale doble. El valor del resto de los dados (de unidades no especializadas en defensa) simplemente se suma como antes.

Ejemplo:

* El jugador que ataca lo hace con una unidad especializada en ataque, una especializada en defensa y una no especializada. Los dados tienen valor 4, 3 y 2 respectivamente. El puntaje de ataque es 2x4 + 3 + 2 = 13.
* El jugador que defiende tiene dos unidades especializadas en defensa y una especializada en recolección. Sus dados tienen valores 3, 2 y 5, respectivamente. El puntaje es 2x3 + 2x2 + 5 = 15.
* Por lo tanto gana la defensa. El jugador atacante decide por ejemplo entregar a su unidad no especializada.

